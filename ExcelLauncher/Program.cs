﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace ExcelLauncher
{
    class Program
    {
        const int SWP_NOSIZE = 0x0001;
        const int SWP_NOMOVE = 0x0002;
        const int SWP_SHOWWINDOW = 0x0040;

        const int HWND_TOPMOST = -1;
        const int HWND_NOTOPMOST = -2;

        enum ShowWindowEnum
        {
            Hide = 0,
            ShowNormal = 1,
            ShowMinimized = 2,
            ShowMaximized = 3,
            Maximize = 3,
            ShowNormalNoActivate = 4,
            Show = 5,
            Minimize = 6,
            ShowMinNoActivate = 7,
            ShowNoActivate = 8,
            Restore = 9,
            ShowDefault = 10,
            ForceMinimized = 11
        };

        static Guid IID_IUnknown = new Guid("00000000-0000-0000-C000-000000000046");
        [DllImport("ole32.dll")]
        static extern int CreateBindCtx(uint reserved, out IBindCtx ppbc);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, ShowWindowEnum nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int uFlags);


        static void Main(string[] args)
        {
#if DEBUG
            var fileName = @"C:\Users\Takanori\Desktop\ex_00_Index.xlsx";
            var sheetName = "手法一覧（横）";
#else
            if (2 > args.Length)
            {
                Console.WriteLine("usage: ExcelLauncher <ExcelFilePath> <SheetName>");
                Environment.Exit(1);
            }
            var fileName = args[0];
            var sheetName = args[1];
#endif
            if (!File.Exists(fileName))
            {
                Console.WriteLine("Not found file:" + fileName);
                Environment.Exit(1);
            }

            var filePath = Path.GetFullPath(fileName);

            // すでに開かれているファイルなら、アクティブにする。
            Workbook book = GetOpenedExcelBook(filePath);
            if (null != book)
            {
                Microsoft.Office.Interop.Excel.Application excel = null;
                try
                {
                    excel = book.Application;
                    ShowExcel(excel);
                    ActiveSheet(book, sheetName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                if (null != book) Marshal.ReleaseComObject(book);
                if (null != excel) Marshal.ReleaseComObject(excel);
                return;
            }

            // 指定ファイルを開く
            Launch(filePath, sheetName);
        }

        private static void Launch(string filePath, string sheetName)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Workbooks books = null;
            Workbook book = null;

            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                ShowExcel(excel);

                books = excel.Workbooks;
                book = books.Open(filePath);

                ActiveSheet(book, sheetName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (null != book) Marshal.ReleaseComObject(book);
            if (null != books) Marshal.ReleaseComObject(books);
            if (null != excel) Marshal.ReleaseComObject(excel);
        }


        /**
         * エクセルの表示と、フォアグラウンドにWindowをもってくる。
         *
         */
        private static void ShowExcel(Microsoft.Office.Interop.Excel.Application excel)
        {
            excel.Visible = true;
            ShowWindow((IntPtr)excel.Hwnd, ShowWindowEnum.Restore);
            ActiveWindowWithSetWindowPos((IntPtr)excel.Hwnd);
        }

        /**
         * 指定ワークシートをアクティブにする 
         */
        private static void ActiveSheet(Workbook book, string sheetName)
        {
            Sheets sheets = null;
            Worksheet sheet = null;

            try
            {
                sheets = book.Sheets;
                sheet = sheets[sheetName];
                sheet.Activate();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (null != sheet) Marshal.ReleaseComObject(sheet);
            if (null != sheets) Marshal.ReleaseComObject(sheets);
        }


        /**
         * 起動中の指定ワークブックを取得する
         */
        private static Workbook GetOpenedExcelBook(string path)
        {
            Workbook retBook = null;
            Workbook[] books = GetOpenedExcelBooks();
            foreach (Workbook book in books)
            {
                // 関係ないものは解放する
                if (null != retBook || path.ToLower() != book.FullName.ToLower())
                {
                    Marshal.ReleaseComObject(book);
                    continue;
                }
                retBook = book;
            }
            return retBook;
        }

        /**
         * 起動中のエクセルで開いているワークブック一覧を取得する
         */
        private static Workbook[] GetOpenedExcelBooks()
        {
            List<Workbook> arr = new List<Workbook>();

            // IBindCtx
            IBindCtx pBindCtx = null;
            CreateBindCtx(0, out pBindCtx);

            // IRunningObjectTable
            IRunningObjectTable pROT = null;
            pBindCtx.GetRunningObjectTable(out pROT);

            // IEnumMoniker
            IEnumMoniker pEnumMoniker = null;
            pROT.EnumRunning(out pEnumMoniker);

            pEnumMoniker.Reset();

            for (; ; )
            {
                // IMoniker
                IMoniker[] pMonikers = { null };

                IntPtr fetched = IntPtr.Zero;
                if (pEnumMoniker.Next(1, pMonikers, fetched) != 0)
                {
                    break;
                }

                // For Debug
                string strDisplayName;
                pMonikers[0].GetDisplayName(pBindCtx, null, out strDisplayName);

                object obj = null;
                Workbook pBook = null;
                try
                {
                    pMonikers[0].BindToObject(pBindCtx, null, ref IID_IUnknown, out obj);

                    pBook = obj as Workbook;
                    if (pBook != null)
                    {
                        arr.Add(pBook);
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    Marshal.ReleaseComObject(pMonikers[0]);
                }

            }

            Marshal.ReleaseComObject(pEnumMoniker);
            Marshal.ReleaseComObject(pROT);
            Marshal.ReleaseComObject(pBindCtx);

            return arr.ToArray();
        }


        /**
         * 指定ハンドルのウィンドウを前面に持ってくる。 
         */
        public static void ActiveWindowWithSetWindowPos(IntPtr hWnd)
        {
            SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
            SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
        }
    }
}
